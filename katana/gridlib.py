# -*- coding: utf-8 -*-
"""Square grid utilities module."""


from __future__ import print_function, division, unicode_literals
import math


__all__ = ('SquareGrid',)
__version__ = 2  # Incremental module version
__author__ = "Joseph Choufani <joseph.choufani.dev@gmail.com>"


try:
    #  Overrides python-2's `range` with `xrange`-behaviour
    range = xrange  # pylint: disable=invalid-name,redefined-builtin
except NameError:
    #  Python-3
    pass


DEFAULT_GRID_SIZE = 9
"""int: Default grid size constant."""


class SquareGrid(object):
    """Base SquareGrid object.

    Grid items are accessed by a tuple indicating their 0-based integer row
    index and their 0-based integer column index. If an integer is provided
    as item access key, it is assumed to refer to the 0-based integer
    sub-square index and returns the list of items in that sub-square.
    Additionally, rows and columns items are returned when tuples containing
    an `Ellipsis` are used to access items.

    Grid items assignment can only be done individually using the tuple item
    index.

    Usage:
        ```
        >>> # Creates a size-4 grid
        >>> grid = SquareGrid(range(4**2), grid_size=4)
        <4x4 katana.katanalib.SquareGrid object>
        >>> print grid
          0  1 |  2  3
          4  5 |  6  7
        -------+-------
          8  9 | 10 11
         12 13 | 14 15
        >>> grid[2, 2] # item access
        10
        >>> grid.item(2, 2) # item access method
        10
        >>> grid.row(1) # row items list
        [4, 5, 6, 7]
        >>> list(grid[1, ...]) # another way for row items list
        [4, 5, 6, 7]
        >>> grid.column(3) # column items list
        [3, 7, 11, 15]
        >>> list(grid[..., 3]) # another way for column items list
        [3, 7, 11, 15]
        >>> list(grid[0]) # sub-square access
        [0, 1, 4, 5]
        >>> list(grid.get_sub(0)) # another way for sub-square access
        [0, 1, 4, 5]
        >>> list(grid.get_item_sub(2, 2)) # item sub-square access
        [10, 11, 14, 15]
        >>> list(iter(grid)) # All items list
        [0, 1, 2, 3, 4 , 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
        >>> grid[2, 2] = 20 # item assignment
        >>> grid[2, 2]
        20
        >>> print(grid)
          0  1 |  2  3
          4  5 |  6  7
        -------+-------
          8  9 | 20 11
         12 13 | 14 15

        ```

    """

    def __init__(self, grid_values, grid_size=DEFAULT_GRID_SIZE):
        """Constructor.

        Args:
            grid_values (:obj:`list` of :obj:`int`): Grid contents.
                List of values by serialised by line of the grid.
                The size of the list must be be the square of the grid size.

            grid_size (:obj:`int`): Grid size.
                Must be a valid square of an integer (default: 9).

        Raises:
            :obj:`ValueError`: Invalid grid size or input values.

        """
        #  Sub-square size is the square root of the grid size
        sub_size = math.sqrt(grid_size)
        if sub_size != int(sub_size):
            raise ValueError("grid size must be valid square of an integer")
        values = list(grid_values) if grid_values is not None else ()
        if not values or len(values) != grid_size ** 2:
            raise ValueError("invalid input grid values")
        self.__grid = values
        self.__size, self.__sub = grid_size, int(sub_size)

    def __getitem__(self, key):
        """Item access.

        Accesses item using its 2-tuple row/col index.
        If an int is given, it's treated as a sub-square index.

        """
        try:
            if isinstance(key, tuple):
                #  Tuple mode
                #  Creates time ranges based on key tuple values
                row_range = range(self.__size) if key[0] is Ellipsis \
                            else range(int(key[0]), int(key[0]) + 1)
                col_range = range(self.__size) if key[1] is Ellipsis \
                            else range(int(key[1]), int(key[1]) + 1)
                #  Checks for item mode
                item_mode = len(row_range) == 1 and len(col_range) == 1

                def _generator():
                    for i in row_range:
                        for j in col_range:
                            yield self.__grid[i * self.__size + j]

                return \
                    self.item(row_range[0], col_range[0]) if item_mode \
                    else _generator()
            #  Non-tuple mode returns sub-square at index `key`.
            return self.get_sub(key)
        except (IndexError, TypeError):
            pass
        #  Defaults to raising :obj:`IndexError`
        raise IndexError(key)

    def __setitem__(self, key, value):
        """Set an item.

        Sets an item at the given 2-tuple row/col index.

        Args:
            key (tuple): 2-tuple item row/col index.
            value: Value to set.

        Raises:
            :obj:`IndexError`: Invalid index type or index out of range.

        """
        try:
            self.__grid[key[0] * self.__size + key[1]] = value
            success = True
        except (IndexError, TypeError):
            success = False
        if not success:
            raise IndexError(key)

    def __repr__(self):
        """Class representation string."""
        return "<{0}x{0} {1}>".format(
            self.__size, object.__repr__(self).strip("<>")
        )

    def __str__(self):
        """Grid print format."""
        grid_str = ""  # Grid string container
        #  Calculates display justify size
        disp_size = 1 + (max(iter(len(str(v)) for v in self.__grid)) or 0)
        for i, val in enumerate(self.__grid):
            #  Finds row and column indices
            row, col = i // self.__size, i % self.__size
            #  Adds column separator
            if not col % self.__sub and col and (col + 1) % self.__size:
                grid_str += " |"
            # Adds row separator
            row_str = "-" * (disp_size * self.__sub + 1)
            if not col and row:
                grid_str += "\n" if row % self.__sub else "\n{}\n".format(
                    (row_str + "+") * (self.__sub - 1) + row_str
                )
            #  Adds value to string
            grid_str += "{0:>{1}}".format(str(val), disp_size)
        #  Returns updated grid string
        return grid_str

    def __contains__(self, value):
        """Check value in grid."""
        return value in self.__grid

    def __iter__(self):
        """Iterate over grid elements."""
        return iter(self.__grid)

    @property
    def size(self):
        """:obj:`int`: Read-only size of the grid."""
        return self.__size

    def item(self, row, col):
        """Get grid item by row and column.

        Args:
            row (int): row index.
            col (int): column index.

        Returns:
            Grid item object.

        Raises:
            :obj:`IndexError`: Invalid index.

        """
        try:
            return self.__grid[row * self.__size + col]
        except (TypeError, IndexError, ValueError):
            pass
        #  Falls back to raising an index error
        raise IndexError((row, col))

    def row(self, row):
        """Get items of a grid row.

        Args:
            row (int): Row index.

        Returns:
            :obj:`list` of object: List of row items.

        Raises:
            :obj:`IndexError`: Invalid row index.

        To-do:
            - Check usage and consider changing returned list to a generator.

        """
        try:
            return [
                self.__grid[row * self.__size + j]
                for j in range(self.__size)
            ]
        except (TypeError, IndexError, ValueError):
            raise IndexError("invalid row index: %s" % row)

    def column(self, col):
        """Get items of a grid column.

        Args:
            col (int): Column index.

        Returns:
            :obj:`list` of object: List of column items.

        Raises:
            :obj:`IndexError`: Invalid column index.

        """
        try:
            return [
                self.__grid[i * self.__size + col]
                for i in range(self.__size)
            ]
        except (TypeError, IndexError, ValueError):
            raise IndexError("invalid column index: %s" % col)

    def get_sub(self, sub_idx):
        """Get elements of a sub-square.

        Args:
            sub_idx (int): 0-based integer sub-square index.

        Returns:
            :obj:`generator`: Generator of items contained in sub-square.

        Raises:
            :obj:`IndexError`: Invalid sub-square index.

        """
        if sub_idx < 0 or sub_idx >= self.__size:
            raise IndexError("index out of range")
        #  Sub-square coordinates
        row, col = sub_idx // self.__sub, sub_idx % self.__sub
        start_idx = self.__sub * (self.__size * row + col)
        #  Returns items generator
        return (
            self.__grid[start_idx + i * self.__size + j]
            for i in range(self.__sub)
            for j in range(self.__sub)
        )

    def get_item_sub(self, row, col):
        """Get element's sub-square neighbours (including itself).

        Args:
            row (int): Item row index.
            col (int): Item column index.

        Returns:
            :obj:`generator`: Generator of items contained in sub-square.

        Raises:
            :obj:`IndexError`: Invalid sub-square index.

        """
        if row < 0 or row >= self.__size or col < 0 or col >= self.__size:
            raise IndexError("invalid range")
        #  Calculates element's sub-square index
        sub_idx = self.__sub * (row // self.__sub) + col // self.__sub
        return self.get_sub(sub_idx)


class GridCell(object):
    """Basic grid cell with backtrack support.

    Uses its internal cell-value with comparison operators and types
    conversion (`int`, `float`, `str`).

    The `in` operator and `len` function operate on the internal stack.

    """

    def __init__(self, value=None):
        """Constructor.

        Args:
            value (optional): Initial value.

        """
        self.__value = None
        self.__stack = set()
        if value is not None:
            #  Uses value setter if value is not None for type checking
            self.value = value

    def __eq__(self, value):
        return self.__value == value

    def __lt__(self, value):
        return self.__value < value

    def __gt__(self, value):
        return self.__value > value

    def __contains__(self, value):
        return value in self.__stack

    def __len__(self):
        return len(self.__stack)

    def __int__(self):
        return int(self.__value) if self.__value is not None else None

    def __float__(self):
        return float(self.__value) if self.__value is not None else None

    def __str__(self):
        return str(self.__value)

    def __hash__(self):
        return hash(self.__value)

    @property
    def value(self):
        """Cell value property.

        Notes:
            - In fact the cell value is type-agnostic but the most common
              type is expected to be `int`.

        """
        return self.__value

    @value.setter
    def value(self, new_val):
        #  Gets raw value
        value = new_val.value if isinstance(new_val, GridCell) else new_val
        #  Sets private field
        self.__value = value
        #  Pushes value to internal stack
        self.push(value)

    @property
    def stack(self):
        """:obj:`setiterator`: Iterator of the values in the stack."""
        return iter(self.__stack)

    @property
    def is_set(self):
        """:obj:`bool`: Boolean flag indicating if the cell is set.

        To-do:
            - Convert into a function.

        """
        return self.__value is not None

    def push(self, value):
        """Add a value into the cell stack.

        Args:
            value: Value to add to stack.

        """
        self.__stack.add(value)

    def clear(self):
        """Clear the cell stack."""
        self.__stack = set()

    def unset(self):
        """Unset the value of the cell."""
        self.__value = None

    def reset(self):
        """Reset the cell.

        Unsets the cell value it and clears its stack.

        """
        self.unset()
        self.clear()
