# -*- coding: utf-8 -*-
"""Grid sequences utilities module."""


from __future__ import print_function, division, unicode_literals


__all__ = ('SequenceStop',)
__version__ = 2  # Incremental module version
__author__ = "Joseph Choufani <joseph.choufani.dev@gmail.com>"


try:
    #  Overrides python-2's `range` with `xrange`-behaviour
    range = xrange  # pylint: disable=invalid-name,redefined-builtin
except NameError:
    #  Python-3
    pass


class SequenceStop(IndexError):
    """Sequence stop signal error class."""


def lrtb_seq_next(curr_idx, limit):
    """Calculate next index in a Left to Right then Top to Bottom sequence.

    Args:
        curr_idx (tuple of int):
            2-tuple of starting 0-based (row, column) indices.
        limit (int): Grid limit.

    Returns:
        :obj:`tuple` of :obj:`int`:
            Next 2-tuple of 0-based (row, column) indices.

    Raises:
        :obj:`IndexError`: Index out of range.
        :obj:`SequenceStop`: Sequence completed.

    """
    if curr_idx[0] > limit or curr_idx[1] > limit:
        raise IndexError(curr_idx)
    if curr_idx[0] == limit and curr_idx[1] == limit:
        raise SequenceStop(curr_idx)
    size = limit + 1
    next_idx = curr_idx[0] * size + curr_idx[1] + 1
    return (next_idx // size, next_idx % size)


def slalom_next(curr_idx, limit, step=1):
    """Calculate next index in a slalom sequence.

    Args:
        curr_idx (tuple of int): 2-tuple of starting (row, column) indices.
        limit (int): Grid limit.
        step (int): Optional sequence step (default: 1).

    Returns:
        :obj:`tuple`: 2-tuple of next 0-based (row, column) indices.

    Raises:
        :obj:`IndexError`: Index out of range.
        :obj:`SequenceStop`: Sequence completed.

    """
    curr_x, curr_y = curr_idx[0], curr_idx[1]
    if curr_x > limit or curr_y > limit:
        #  Index out of range
        raise IndexError(curr_idx)
    #  Direction sign
    sign = -1 if curr_x % 2 else +1
    #  Current distance to relative edge
    dist = (limit - curr_y) if sign > 0 else curr_y
    if dist >= step:
        #  Moves 1 step on the same row following direction
        return (curr_x, curr_y + sign*step)
    else:
        #  If step is larger than distance, continues on next row
        next_x = curr_x + 1
        if next_x > limit:
            #  Last row reached -> sequence completed
            raise SequenceStop
        #  Completes step starting from edge
        delta = step - dist - 1
        next_y = (limit - delta) if sign > 0 else delta
        return (next_x, next_y)
