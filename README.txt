#   Sudoku Katana

Python3 Sudoku utilities library


##  Installation

### 1.  Submodule

To install the library as a submodule of your project use the following
command:

```
git submodule add https://gitlab.com/joseph.choufani/katana.git
```

To use the library in your code you need to access via the `katana.katana`
subpackage. Example:

```python
from katana.katana import katanalib as kl

#  Creates a grid
grid = kl.SudokuPuzzle.random()
```

### 2.  Disutils

To install the library using Distutils setup utilities, download the source
files or clone the using the command:

```
git clone https://gitlab.com/joseph.choufani/katana.git
#  Go into the source directory
cd katana
```

Inside the source directory `katana`, execute the following command to install
the `katana` package:

```
python setup.py install
```

To use the library in your code simply import the `katana` pakage. Example:

```python
from katana import katanalib as kl

#  Creates a grid
grid = kl.SudokuPuzzle.random()
```

##  Credit

Developed by: Joseph Choufani <joseph.choufani.dev@gmail.com>
