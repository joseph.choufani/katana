# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

[//]: # (Maintainer comments are added using this line's format.)

## [2.0.0] - 2018-05-26
### Added
-   Distutils setup file [setup.py](setup.py).

### Changed
-   Re-arranged structure into sub-package [`katana`](katana/) with submodule
    compatibility.
-   Incremented [`katanalib`](katana/katanalib.py) module version to 9.
-   Incremented [`gridlib`](katana/gridlib.py) module version to 2.
-   Incremented [`sequences`](katana/sequences.py) module version to 2.

##  [1.2.0] - 2018-04-26
### Added
-   Grid utilities module [`gridlib`](gridlib.py).
-   Sequence utilities module [`sequences`](sequences.py).

### Changed
-   Python-2/Python-3 compatibility for all modules.
-   Minor performance improvement changes.

## [1.1.0] - 2017-05-03
### Added
- Value swap, column swap, blocks swap and grid roll methods.
- Random propagation to randomly select propgation methods and apply them.

## [1.0.0] - 2017-03-22
### Changed
- Top-level package structure for submodule release.

## [0.5.0] - 2017-03-17
### Changed
- User-controllable puzzle generation and solving.

## [0.4.0] - 2017-03-12
### Added
- Item, row and column access via corresponding `SquareGrid` methods.
- Listing all `SquareGrid` items via built-in `iter` function.

## [0.3.1] - 2017-02-24
### Changed
- Randomly fix candidates relative to cell stack when checking for unique solution.
- Hole-digging sequence can be forced to randomisation using optional `force_random` parameter in `SudokuPuzzle.random`

## [0.3.0] - 2017-02-23
### Added
- Left to Right then Top to bottom and Slalom grid sequences indexes calculators.
- Solver method `SudokuPuzzle.solve`.
- Implemented unique solution checker function.

### Changed
- Hole-digging sequence determination based on difficulty level.
- Hole-digging acceptance dependent of uniqueness of solution.

## [0.2.0] - 2017-02-22
### Added
- Unset method to `katana.katanalib.GridCell` base class.
- Sub-Class `katana.katanalib.PuzzleCell` of `katana.katanalib.GridCell` adapted to puzzle generation using the digging-holes method.
- Basic sub-class `katana.katanalib.SudokuPuzzle` of `katana.katanalib.SquareGrid` to wrap Sudoku puzzles with `random` method to randomly generate a puzzle based on level and size.

## [0.1.0] - 2017-02-21
### Added
- Base `katana.katanalib.SquareGrid` class with special items access and individual items assignment methods.
- Class `katana.katanalib.GridCell` to wrap grid cells with backtrack support.
- Class `katana.katanalib.SudokuGrid` to validate a grid with a the `random` class method that randomly generates a full Sudoku grid.
